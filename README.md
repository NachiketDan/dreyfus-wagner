**Dreyfus-Wagner algorithm for the Steiner problem**

Dreyfus-Wagner algorithm is to find the shortest path which must include a specific number of fixed points called terminals. Terminal nodes are a subset of the nodes in the graph.

SteinerTree.cpp contains the implementation for Dreyfus-Wagner algorithm.
Dijkstra.cpp contains the code to calculate the shortest path from every node to every another node. The shortest path matrix is an input to the Dreyfus-Wagner algorithm.

Tested for a complete graph of 7 nodes, with 4 and 5 terminals.

---